import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-altamotofoto',
  templateUrl: './altamotofoto.page.html',
  styleUrls: ['./altamotofoto.page.scss'],
})
export class AltamotofotoPage implements OnInit {


  marca;
  modelo;
  anyo;
  precio;
  image;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  add(){
    this.image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    const form = new FormData();
    form.append("foto","");
    form.append("marca",this.marca);
    form.append("modelo",this.modelo);
    form.append("year",this.anyo);
    form.append("precio",this.precio);

    //const url = "http://motos.puigverd.org:80/moto/foto";
    const url = "http://kevin-barzallo-7e2.alwaysdata.net/miapi/moto";

    fetch(url, {
      "method": "POST",
      "body": form
    })
    .then(response => {
      this.router.navigateByUrl('/home');
    })
    .catch(err => {
      console.log(err);
    });
  }

}
