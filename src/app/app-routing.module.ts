import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'detalle-moto',
    loadChildren: () => import('./detalle-moto/detalle-moto.module').then( m => m.DetalleMotoPageModule)
  },
  {
    path: 'altamoto',
    loadChildren: () => import('./altamoto/altamoto.module').then( m => m.AltamotoPageModule)
  },
  {
    path: 'altamotofoto',
    loadChildren: () => import('./altamotofoto/altamotofoto.module').then( m => m.AltamotofotoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
