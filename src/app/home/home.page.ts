import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  motos;
  //urlMotosApi = "http://motos.puigverd.org/motos";
  urlMotosApi = "http://kevin-barzallo-7e2.alwaysdata.net/miapi/motos";


  constructor(private router:Router, private menu: MenuController) {
    this.getJson(this.urlMotosApi);
  }

  async getJson(url:string){
    const respuesta = await fetch(url);
    this.motos= await respuesta.json();;
  }

  verMoto(moto){

    let navigationExtras: NavigationExtras = {
      state: {
        parametros: moto,
      }
    };
    this.router.navigate(['detalle-moto'], navigationExtras);

  }

  addMoto(){
    this.router.navigate(['altamotofoto']);
  }

  ionViewWillEnter() {
    this.getJson(this.urlMotosApi);
  }

  filter(marca:string){
    if(marca === "todas"){
      this.getJson(this.urlMotosApi);
    }else{
      //this.getJson("http://motos.puigverd.org/motos?marca=" + marca);
      this.getJson("http://kevin-barzallo-7e2.alwaysdata.net/miapi/motos?marca=" + marca);

    }
    this.menu.close();
  }



  

}
