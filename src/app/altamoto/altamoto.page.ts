import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-altamoto',
  templateUrl: './altamoto.page.html',
  styleUrls: ['./altamoto.page.scss'],
})
export class AltamotoPage implements OnInit {

  marca;
  modelo;
  anyo;
  precio;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  add(){
    console.log("hola");
    let data = {
      "marca": this.marca,
      "modelo": this.modelo,
      "year": this.anyo,
      "foto": "",
      "precio": this.precio
    }

    const url = "http://kevin-barzallo-7e2.alwaysdata.net/miapi/moto";
    fetch(url, {
      "method": "POST",
      "body": JSON.stringify(data),
      "headers": {"Content-type": "application/json; charset=UTF-8"}

    })
    .then(response => {
      this.router.navigateByUrl('/home');
    })
    .catch(err => {
      console.log(err);
    });
  }

}
