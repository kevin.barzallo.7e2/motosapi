import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-moto',
  templateUrl: './detalle-moto.page.html',
  styleUrls: ['./detalle-moto.page.scss'],
})
export class DetalleMotoPage implements OnInit {

  moto;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.moto = this.router.getCurrentNavigation().extras.state.parametros;
      }
    });
  }

  async presentAlert(idMoto) {
    console.log(idMoto);

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cuidado!',
      message: '<strong>Estas seguro que lo quieres eliminar?</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Vale',
          handler: () => {
            this.deleteMoto(idMoto);
          }
        }
      ]
    });

    await alert.present();

  }

  deleteMoto(id){
    console.log(id);
    //const url = "http://motos.puigverd.org/moto/" + id;
    const url = "http://kevin-barzallo-7e2.alwaysdata.net/miapi/moto/" + id;

    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
      this.router.navigateByUrl('/home');
    });

    
  }

  ngOnInit() {
  }

}
