import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleMotoPageRoutingModule } from './detalle-moto-routing.module';

import { DetalleMotoPage } from './detalle-moto.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleMotoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DetalleMotoPage]
})
export class DetalleMotoPageModule {}
